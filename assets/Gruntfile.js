/***
 *      http://www.laravel.com.br/usando-grunt-com-laravel-e-bootstrap/
 *      Install
 npm install grunt --save-dev 
 npm install grunt-contrib-concat --save-dev 
 npm install grunt-contrib-less --save-dev 
 npm install grunt-contrib-uglify --save-dev 
 npm install grunt-contrib-watch --save-dev 
 * 
 ***/


module.exports = function(grunt) {

    //Inicializando o objeto de configuração
    grunt.initConfig({
        less: {
            development: {
                options: {
                    compress: false   //minifica o resultado
                },
                files: {
                    "../public/css/style.css": "./stylesheets/base.less"
                }
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            javascript: {
                src: [
                    './javascript/funcoes.js',
                    //'./javascript/funcoes-otro.js'
                ],
                dest: '../public/js/app.js'
            },
            javascriptLib: {
                src: [
                    './javascript/libs/*.js',
                ],
                dest: '../public/js/libs.js'
            }
        },
        uglify: {
            options: {
                mangle: false  // não muda os nomes das funções e variáveis
            },
            dist: {
                files: {
                    '../public/js/app.min.js': '../public/js/app.js',
                    '../public/js/libs.min.js': '../public/js/libs.js'
                }
            }
        },

        imagemin: {
            png: {
                options: {
                    optimizationLevel: 7
                },
                files: [
                    {
                        // Set to true to enable the following options…
                        expand: true,
                        // cwd is 'current working directory'
                        cwd: './images/',
                        src: ['**/*.png'],
                        // Could also match cwd line above. i.e. project-directory/img/
                        dest: '../public/img/',
                        ext: '.png'
                    }
                ]
            },
            jpg: {
                options: {
                    progressive: true
                },
                files: [
                    {
                        // Set to true to enable the following options…
                        expand: true,
                        // cwd is 'current working directory'
                        cwd: './images/',
                        src: ['**/*.jpg'],
                        // Could also match cwd. i.e. project-directory/img/
                        dest: '../public/img/',
                        ext: '.jpg'
                    }
                ]
            }
        },
        watch: {
            js: {
                files: ['./javascript/*.*'], //arquivos monitorados
                tasks: ['concat:javascript', 'uglify'], //tarefas executadas
                options: {
                    livereload: true                        //atualiza o navegador
                }
            },
            less: {
                files: ['./stylesheets/*.*'], //arquivos monitorados
                tasks: ['less'], //tarefas executadas
                options: {
                    livereload: true                        //atualiza o navegador
                }
            },
            views: {
                files: ['../public/**/*.php', '../app/views/**/.php'], //arquivos monitorados          
                options: {
                    livereload: true                        //atualiza o navegador
                }
            }
            //tests: {
            //  files: ['app/controllers/*.php','app/models/*.php'],  //a tarefa vai ser executada só quando salvar arquivo nessa localização
            // tasks: ['phpunit']
            //}          
        }
    });

    // Carregar os plugins
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    //grunt.loadNpmTasks('grunt-imageoptim');
    //grunt.loadNpmTasks('grunt-phpunit');
    grunt.loadNpmTasks('grunt-contrib-imagemin');


    // Definicão da tarefa default
    grunt.registerTask('default', ['concat', 'less', 'uglify', 'watch']);

};
