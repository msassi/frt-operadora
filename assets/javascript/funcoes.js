/* LOAD */
$(window).ready(function()
{
    
    // RANGER PRECO
    $( "#slider-range" ).slider({
          range: true,
          min: 0,
          max: 500,
          values: [ 75, 300 ],
          slide: function( event, ui ) {
            $( "#amount" ).val( "R$ " + ui.values[ 0 ] + " à R$ " + ui.values[ 1 ] );
          }
    });
    $( "#amount" ).val( "R$ " + $( "#slider-range" ).slider( "values", 0 ) + " à R$ " + $( "#slider-range" ).slider( "values", 1 ) );

    // RANGER Horário
    $( "#slider-range-horario" ).slider({
        range: true,
        min: 0,
        max: 23,
        values: [ 0, 23 ],
        slide: function( event, ui ) {
            $( "#time" ).val( ui.values[ 0 ] + "h  às " + ui.values[ 1 ] + "h" );
        }
    });
    $( "#time" ).val(  $( "#slider-range-horario" ).slider( "values", 0 ) + "h às " + $( "#slider-range-horario" ).slider( "values", 1 ) + "h" );

   
    $('.topo').click(function(){
	$("html, body").animate({scrollTop: 0}, "slow");
	return false;
    });
    
    
    // carrocel
    $('#carousel-destaque').carousel(); 
       
       
    $('#tabmotor a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });


    /** SCROLLING AEREO **/
    // Config
    // =================================================

    var $nav_header       = $('#header'),
        $nav_head         = $('.resultbusca>.head'),
        $login_header     = $('#header .login'),
        $content_sidebar  = $('.sidebar-fix-top'),
        list_aereo        = $('#list-aereo, #list-hotel').height() - 200,
        header_height     = $nav_header.height(),
        hero_height       = 300,// $('.hero').height(),
        offset_val        = hero_height - header_height;


    // Method
    // =================================================

    function navSlide() {
        var scroll_top = $(window).scrollTop();

        if (scroll_top >= offset_val) { // the detection!
            $login_header.hide();
            $nav_head.addClass('fixed-top');

            if(list_aereo > scroll_top){
                $content_sidebar.css('margin-top', scroll_top);
            }

        } else {
            $login_header.show();
            $nav_head.removeClass('fixed-top');
            $content_sidebar.css('margin-top', 12);
        }

    }

    // Handler
    // =================================================
    if ($(document).width() > 800 && $('#fix-top, #hotel').data('enable') == 1) { // em telas pequenas como mobile, não deve ser executado
            $(window).scroll(navSlide);
    }
    //Quando mudar o tamanho da tela, causado pelas requisições ajax
    $( window ).resize(function(){
            $nav_header       = $('#header'),
            $nav_head         = $('.resultbusca>.head'),
            $login_header     = $('#header .login'),
            $content_sidebar  = $('.sidebar-fix-top'),
            list_aereo        = $('#list-aereo, #list-hotel').height() - 200,
            header_height     = $nav_header.height(),
            hero_height       = 300,// $('.hero').height(),
            offset_val        = hero_height - header_height;
            navSlide();
    });


    // Somente na home os icones devem ficar ativos
    if( window.location.toString().indexOf('home.php') < 0){
        $('ul.navbar-nav.menu li').each(function(){
            $(this).find('span').removeClass('active');
        });
    }

    $('.resultado .item-aereo').each(function(){
        $(this).parent('.col-md-12').find('.total').css('height', $(this).height());

        var pt = ( ($(this).height()-210)/2);

        if(pt > 0) {
            $(this).parent('.col-md-12').find('.total').css('padding-top', pt);
        }
    });



    
});

