<footer id="footer">
    <div class="newsletter">
        <div class="container">                                
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <span class="cvg iconnews"></span>                    
                    <p class="title">Receba com exclusividade nossas novidades e promoções por e-mail</p>                          
                </div>
                <div class="col-md-8 col-sm-8">
                    <form class="form-inline" role="form">                       
                        <div class="pull-left">                          
                            <label for="cidade">Cidade</label>
                            <input type="text" class="form-control" id="cidade">
                        </div>
                        <div class="pull-left">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control" id="email">
                        </div>   
                        <div class="pull-left">
                            <button type="submit" class="btn btn-default btnyellow">
                                <p>Assinar</p>
                                <span class="cvg iconsubmitnews"></span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>                          
        </div><!-- .container -->
    </div><!-- .newsletter -->
    
    <div class="container">
        <div class="row">
            <div class="col-md-12">                
                <div class="fones center-block">
                    <div class="pull-left hidden-xs"><span class="cvg fone"></span></div>
                    <div class="pull-left">
                        <p>Foz do Iguaçu</p>
                        <p>+55 (45) 3521-8500</p>
                    </div>
                    <span class="cvg setagray"></span>
                    <div class="pull-left">
                        <p>Curitiba</p>
                        <p>+55 (45) 3521-8500</p>
                    </div>
                    <span class="cvg setagray"></span>
                    <div class="pull-left">
                        <p>Londrina</p>
                        <p>+55 (45) 3521-8500</p>
                    </div>
                    <span class="cvg setagray"></span>
                    <div class="pull-left">
                        <p>Maringá</p>
                        <p>+55 (45) 3521-8500</p>
                    </div>
                    <span class="cvg setagray"></span>
                    <div class="pull-left">
                        <p>Porto Alegre</p>
                        <p>+55 (45) 3521-8500</p>
                    </div>
                </div>                    
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="info">
                    <p>FRTOPERADORA.COM.BR - Ministério do Turismo - Cadastur 18.038059.10.0001-6 Copyright 2014, FRT Operadora de Turismo. Todos os direitos reservados.</p>
                    <p>Av. Brasil, 720 - Sala 101 - Centro - CEP: 85851-000 - Foz do Iguaçu - PR - Brasil.</p>
                    <p style="margin-top: 15px">Powered by geniusws.com.br © 2014</p>
                    
                    <span class="cvg logofrtbase"></span>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="visible-xs">
                <a href="#topo" class="topo">
                    
                        <span class="glyphicon glyphicon-chevron-up"></span>   
                        
                    
                </a> 
            </div>
        </div>
    </div>
</footer>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="js/jquery-1.10.2_1.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/libs.min.js"></script>
<script src="js/app.js"></script>
    
</body>

</html>