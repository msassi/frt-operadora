<?php include 'header.php'; ?>            



<div id="gerar-orcamento" class="resultbusca">
    <div class="head">
        <div class="container">
            <div class="row">         
                <div class="col-md-9 col-md-offset-3 col-sm-8 col-xs-12">
                    <ul class="menuresult">
                        <li >                            
                            <span class="cvg menuresultcvg aereo hidden-xs active"></span>
                            <p>Aéreo</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li>                            
                            <span class="cvg menuresultcvg hotel hidden-xs"></span>
                            <p>Hotel</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li>
                            <span class="cvg menuresultcvg servicos hidden-xs"></span>
                            <p>Serviços</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li class="active">                            
                            <span class="cvg menuresultcvg orcamento hidden-xs"></span>
                            <p>Orçamento</p>                                                            
                        </li>                        
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- head --> 

    <div class="container">
        <div class="row row-offcanvas row-offcanvas-left">
            <div  class="col-md-9 col-md-offset-3 col-sm-8 col-xs-12">

                <p class="pull-left togglefiltro visible-xs">
                    <button type="button" data-toggle="offcanvas">
                        <span class="glyphicon glyphicon-filter"></span>
                        Filtre sua pesquisa
                    </button>
                </p>

                <div class="clearfix"></div>

                <div class="resultado">
                    <div class="row bg-azul">
                        <div class="col-md-6">
                            <h3>Opções gerais</h3>  
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-default btnrosa">
                                <p>Gerar orçamento/Enviar por e-mail</p>
                                <span class="glyphicon glyphicon-play"></span>
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bg-cinsa bg-cinsa-menor">
                                <h4>Trechos Aéreos</h4> 
                            </div>
                        </div>
                    </div>

                    <div class="row ">

                        <div class="col-md-12">
                            <ul class="item">
                                <li>                                   
                                    <div class="pull-left logo-cia-aerea">
                                        <img src="img/logo-cia-aerea-gol.jpg" class="img-responsive">
                                    </div>
                                    <div class="pull-left info clear">
                                        <p>IGU - Foz do Iguaçu</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>CWB - Curitiba</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Base T</p>
                                        <p>Dur: 00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Escalas 0</p>
                                    </div>                                    
                                </li>
                                <li>
                                    <div class="pull-left logo-cia-aerea">
                                        <img src="img/logo-cia-aerea-gol.jpg" class="img-responsive">
                                    </div>
                                    <div class="pull-left info clear">
                                        <p>IGU - Foz do Iguaçu</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>CWB - Curitiba</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Base T</p>
                                        <p>Dur: 00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Escalas 0</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="pull-left logo-cia-aerea">
                                        <img src="img/logo-cia-aerea-gol.jpg" class="img-responsive">
                                    </div>
                                    <div class="pull-left info clear">
                                        <p>IGU - Foz do Iguaçu</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>CWB - Curitiba</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Base T</p>
                                        <p>Dur: 00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Escalas 0</p>
                                    </div>
                                </li>
                            </ul>
                            <div class="total">
                                <h3>Valor do Aéreo</h3>

                                <p class="price">
                                    R$<strong> 4.919,00</strong> 
                                </p>

                                <p class="obs">
                                    <strong>OBS:</strong> não incluso taxas e encargos
                                </p>

                                <button type="submit" class="btn btn-default btnred">
                                    <p>Assinar</p>
                                    <span class="glyphicon glyphicon-pushpin"></span>
                                </button>
                            </div>
                        </div>



                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="bg-cinsa">
                                <h4>Opção 1</h4> 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="item hotel-orcamento">
                                <li><div class="row">
                                        <div class="col-md-6">
                                            <div class="pull-left imagem">
                                                <img src="img/imghotel.jpg" class="img-responsive">
                                            </div> 
                                        </div>
                                        <div class="col-md-6">
                                            <div class="pull-left info">
                                                <h2>Pontal do Ocaporã</h2>
                                                <p>Sítio Costa Tropicana</p>
                                                <a href="#">Ver no mapa</a>
                                                <div class="classificacao">
                                                    <p class="green">On line</p>
                                                    <span class="cvg starsmall star"></span>
                                                    <span class="cvg starsmall starempty"></span>
                                                    <span class="cvg starsmall starempty"></span>
                                                    <span class="cvg starsmall starempty"></span>
                                                    <span class="cvg starsmall starempty"></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="text-content">
                                                    <p class="desc-hotel">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt, mollitia, obcaecati sed adipisci blanditiis eum assumenda expedita animi reiciendis quibusdam! Temporibus...</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </li>   
                            </ul>  
                            <div class="pull-right total">
                                <h3>À partir de</h3>
                                <p class="price">
                                    R$<strong> 4.919,00</strong> 
                                </p>
                                <a href="#">
                                    <p class="obs">
                                        <span class="glyphicon glyphicon-th"></span>
                                        Opções de Quarto
                                    </p>
                                </a>
                                <button type="submit" class="btn btn-default btnred">
                                    <p>Selecionar</p>
                                    <span class="glyphicon glyphicon-pushpin"></span>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <table class="bg-cinsa">
                            <thead>
                                <tr>
                                    <td>Qtd</td>
                                    <td>Pax</td>
                                    <td>Apartamento</td>
                                    <td>Status</td>
                                    <td>Alimentação</td>
                                    <td>Moeda</td>
                                    <td>Valor</td>
                                </tr>
                            </thead>
                            <tr>
                                <td>1</td>
                                <td>AA</td>
                                <td>Duplo</td>
                                <td>Esperando confirmação</td>
                                <td>Incluso</td>
                                <td>Real</td>
                                <td>R$ 4.000,00</td>
                            </tr>
                        </table>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="bg-cinsa">
                            <h4>Serviços</h4> 
                        </div>
                        <div class="body bg-cinsa">
                            <form class="form-inline " role="form">
                                <div class="row titulo-azul">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Ofertas receptivos</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Moeda</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Total</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label></label>
                                        </div>
                                    </div>

                                </div>  
                                <hr class="boder-dotted">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Bolsa FR
                                                </label>
                                            </div>                                            </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label> BRL </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label> 54,06 </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group input-data">
                                            <input type="date" class="form-control"  placeholder="dd/mm/aa">
                                        </div>
                                    </div>
                                </div>
                                <hr class="boder-dotted">
                                <div class="row ">
                                    <div class="col-md-4 col-sm-6">
                                        <div class="form-group">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Bolsa FR
                                                </label>
                                            </div>                                            </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label> BRL </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label> 54,06 </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group input-data">
                                            <input type="date" class="form-control"  placeholder="dd/mm/aa">
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div class="bg-cinsa">
                            <h4>Opções de pagamento</h4> 
                            <div class="opcao-de-pagamento">
                                <h5>Opção 1</h5>
                                <h6>Hostel Porto do Sol A+ Terrestre + Aéreo</h6>
                                <hr class="boder-dotted">
                                <h6>Entrada 30/10/2014 e Saída 06/11/2014</h6>
                                <p><b>Faturado</b><br />
                                    Faturado- 1 parcela de R$ 1715,64 + TAXASS
                                </p>
                                <p><b>Cheque</b><br />
                                    em até 9 x sem juros-máximo de 9 parcelas totalizando R$ 1715,64 e parcela mínima de R$ 100,00 + TAXASS
                                </p>
                                <p><b>Cartão de crédito</b><br />
                                    Visa, MasterCard e American Express- máximo de 9 parcelas totalizando R$ 1715,64 e parcela mínima de R$ 100,00 + TAXASS
                                </p>
                                
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-6 ">
                            <button type="submit" class="btn btn-default btnrosa pull-right">
                                <p>Gerar orçamento/Enviar por e-mail</p>
                                <span class="glyphicon glyphicon-play"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- row -->
    </div><!-- container -->





</div><!-- .resultbusca -->            

<?php include 'footer.php'; ?>