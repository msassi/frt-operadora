<?php include 'header.php'; ?>            

<div id="cadastre-sua-agencia" class="resultbusca">

    <div class="head">
        <div class="container">                           
            <div class="bg-azul">
                <span class="cvg pessoas"> </span>
                <h3>Cadastre sua agência</h3>
            </div>                            
        </div>
    </div><!-- head --> 

    <div class="container">
        <p><strong>Faça como centenas de agentes ja fizeram, cadastre a sua agência! </strong><br />
            Preencha corretamente os campos abaixo e clique em Cadastrar. * Campos obrigatórios.</p>
        <div class="row">         
            <div class="col-md-7 col-sm-4 hidden-xs">
                <form class="cadastre-agencia">
                    <div class="form-group">
                        <label>Razão social:*</label>
                        <input type="text" class="form-control campo-medio">
                    </div>
                    <div class="form-group">
                        <label>Nome fantasia:*</label>
                        <input type="text" class="form-control campo-medio">
                    </div>
                    <div class="form-group">
                        <label>Pais:</label>
                        <select class="form-control campo-curto">
                            <option value="brasil">Brasil</option>
                            <option value="paraguai">Paraguai</option>
                            <option value="agertina">Argentina</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>CNPJ:*</label>
                        <input type="text" class="form-control campo-curto">
                    </div>
                    <div class="form-group">
                        <label>Cad. Ministério do Turismo:*</label>
                        <input type="text" class="form-control campo-curto">
                    </div>
                    <div class="form-group">
                        <label>Endereço:*</label>
                        <input type="text" class="form-control campo-medio">
                    </div>
                    <div class="form-group">
                        <label>Número:*</label>
                        <input type="text" class="form-control campo-curto">
                    </div>
                    <div class="form-group">
                        <label>Complemento:*</label>
                        <input type="text" class="form-control campo-curto">
                    </div>
                    <div class="form-group">
                        <label>Bairro:*</label>
                        <input type="text" class="form-control campo-medio">
                    </div>
                    <div class="form-group">
                        <label>CEP:*</label>
                        <input type="text" class="form-control campo-curto">
                    </div>
                    <div class="form-group">
                        <label>Cidade:*</label>
                        <input type="text" class="form-control campo-medio">
                    </div>
                    <div class="form-group">
                        <label>Estado:</label>
                        <select class="form-control campo-curto">
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AM">Amazonas</option>
                            <option value="AP">Amapá</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espirito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="PR">Paraná</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SE">Sergipe</option>
                            <option value="SP">São Paulo</option>
                            <option value="TO">Tocantins</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Telefone:*</label>
                        <input type="text" class="form-control campo-curto">
                    </div>
                    <div class="form-group">
                        <label>Contato Principal da Agência:*</label>
                        <input type="text" class="form-control campo-medio">
                    </div>
                    <div class="form-group">
                        <label>E-mail:*</label>
                        <input type="text" class="form-control campo-medio">
                    </div>
                    <div class="form-group">
                        <label>Website:*</label>
                        <input type="text" class="form-control campo-medio">
                    </div>
                    <div class="form-group">
                        <label>Logo da agência. (De preferência 160x80px e nos formatos .bmp/ .jpg/  .gif)</label>
                        <input type="file" id="exampleInputFile" class="form-control campo-curto">
                    </div>
                    <div class="form-group">
                        <label>Como conheceu a FRT:*</label>
                        <select class="form-control campo-curto">
                            <option value="indicacao">Indicação</option>
                            <option value="site">Site</option>
                            <option value="anuncio">Anúncio</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Escolha a Operaodra FRT que prefere se Reportar:*</label>
                        <select class="form-control campo-curto">
                            <option value="foz-do-iguacu">Foz do Iguaçu</option>
                            <option value="cascavel">Cascavel</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Informe o Número de Pessoas que Terão Acesso ao Reserva Online:*</label>
                        <select class="form-control campo-curto">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>
                            <input type="checkbox"> Estou de acordo com os <a href="#">Termos e condições</a> de uso dos sistemas fornecidos pela FRT.
                        </label>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <a href="#">Limpar todos os campos</a>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-black pull-right">Cadastrar Agência</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="conteudo-lateral">
                    <div class="contet-texto">
                        <h2>Porque ter a FRT como sua operadora?</h2>
                    </div>
                    <div class="contet-texto">
                        <h4>Fidelidade ao agente de viagens </h4>
                        <p>Um dos princípios da FRT Operadora é o atendimento prestado exclussivamente ao agente</p>
                        
                        <h4>Agilidade no atendimento</h4>
                        <p>Com a FRT Operadora o agente de viagens tem um acesso direto aos nossos consultores, permitindo maior qualidade de atendimento e agilidade nas suas cotações.</p>
                        
                        <h4>Qualidade nos produtos:</h4>
                        <p>Roteiros e fornecedores muito bem selecionados fazem com que a FRT tenha um alto índice  de satisfação em suas viagens, fornecendo todo o suporte que seu passageiro precisa. </p>
                        
                        <h4>Roteiros presonalizados</h4>
                        <p>Com a FRT, o seu cliente define o melhor roteiro, escolhendo os destinos, a categoria da hospedagem, o transporte, passeios e muito mais.</p>
                        
                        <h4>Parceria</h4>
                        <p>Oferecemos periodicamente programas de capacitação aos Agentes de viagens, Famtours, premiações e incentivo a vendas.</p>
                        
                        <h4>Ótimos preços e flexibilidade de pagamento</h4>
                        <p>Dispomos de negociações exclussivas, com fornecedores em todos os continentes, o que nos permite oferecer pacotes com exelente custo benefício, alémd e ótimas condições de pagamento com cartões de crédito, cheque pré datado e outras formas de financiamento.</p>
                        
                        <h4>Tecnologia e vendas online</h4>
                        <p>A FRT oferece sistemas de vendas online, para compras de passagens aéreas, nacionais e internacionais, hotelaria e serviços integrados com os melhores provedores do mundo, tudo em tempo real e com acesso exclusivo do agente de viagens.</p>
                        
                        <h4>Comunicação e e-marketing</h4>
                        <p>Diariamente enviamos informativos com novidades e as melhores promoções através de e-mail marketing, com o objetivo de sempre de manter atualizados nosso clientes.</p>
                        
                        <h4>Entre com contato conosco!</h4>
                        <p>Teremos maior prazer em atendê-los.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <div class="barra-cinsa">
                    
                </div>
            </div>
        </div>
    </div><!-- container -->





</div><!-- .resultbusca -->            

<?php include 'footer.php'; ?>