<form class="form-inline" role="form">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="form-group">
                    <label>Destino</label>
                    <input type="text" class="form-control" id="destino" placeholder="Digite o nome da cidade">
                </div>
            </div>                                                    
        </div>

        <div class="row">            
            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <label>Entrada</label>
                    <input type="date" class="form-control" id="dataIda" placeholder="dd/mm/aa">
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <label>Saída</label>
                    <input type="date" class="form-control" id="dataVolta" placeholder="dd/mm/aa">
                </div>
            </div>

        </div>


        <div class="row">            
            <div class="col-md-4 col-sm-3">
                <div class="form-group">
                    <label>Quantidade</label>
                    <select class="form-control">
                        <option>1 quarto</option>
                        <option>2 quartos</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 col-sm-3">
                <div class="form-group">
                    <label>Adultos</label>
                    <select class="form-control">
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 col-sm-3">
                <div class="form-group">
                    <label>Crianças</label>
                    <select class="form-control">
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                    </select>
                </div>
            </div>

        </div>

        <div class="row">                                                                                                        
            <div class="col-md-4 col-sm-4">
                <div class="form-group">
                    <label>Adultos</label>
                    <select class="form-control">
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="form-group">
                    <label>Crianças</label>
                    <select class="form-control">
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="form-group">
                    <label>Idades das crianças</label>
                    <select class="form-control">
                        <option>0</option>
                        <option>1</option>
                        <option>2</option>
                    </select>
                </div>
            </div>

        </div>
       
        <div class="row">      
            <div class="col-md-4 pull-right">
                <button type="submit" class="btn btn-default btnyellow">
                    <p>Buscar</p>
                     <span class="glyphicon glyphicon-search"></span>
                </button>
            </div>
        </div>

</form>