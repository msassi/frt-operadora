<?php include "header.php"; ?>

<div id="home">        
    <div class="destaquemotor">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="carousel-motor" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <li data-target="#carousel-motor" data-slide-to="0" class="active"></li>
                          <li data-target="#carousel-motor" data-slide-to="1"></li>
                          <li data-target="#carousel-motor" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                          <div class="item active">
                              <img src="img/banners/maceio.jpg" alt="...">      
                          </div>    
                          <div class="item">
                              <img src="img/banners/banner2.jpg" alt="...">      
                          </div>  
                          <div class="item">
                              <img src="img/banners/maceio.jpg" alt="...">      
                          </div> 
                        </div>

                        <!-- Controls -->
                      <!--  <a class="left carousel-control" href="#carousel-motor" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-motor" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>-->
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div id="motor">
                                <div class="head">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" id="tabmotor">
                                        <li class="active"><a href="#areohotel" data-toggle="tab"><span class="markcircle"></span>Aéreo + Hotel</a></li>
                                        <li><a href="#hotel" data-toggle="tab"><span class="markcircle"></span>Hotel</a></li>
                                        <li><a href="#pacotesonline" data-toggle="tab"><span class="markcircle"></span>Pacotes on-line</a></li>                        
                                    </ul>                       
                                </div><!-- head -->

                                <div class="body">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="areohotel">
                                            <form class="form-inline" role="form">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="optionsradio" id="ida" value="ida" checked> Ida
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionsradio" id="idaEvolta" value="idaEvolta"> Ida e Volta
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionsradio" id="surface" value="surface"> Surface
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <label class="radio-inline">
                                                          <input type="radio" name="optionsradio" id="multiplostrechos" value="multiplostrechos"> Multiplos Trechos
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Origem</label>
                                                            <input type="text" class="form-control" id="origem" placeholder="Digite o nome">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Destino</label>
                                                            <input type="text" class="form-control" id="destino" placeholder="Digite o nome">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Data Ida</label>
                                                            <input type="date" class="form-control" id="dataIda" placeholder="dd/mm/aa">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Data Volta</label>
                                                            <input type="date" class="form-control" id="dataVolta" placeholder="dd/mm/aa">
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Resultados Aéreo</label>
                                                            <select class="form-control">
                                                                <option>100</option>
                                                                <option>2</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Quantidade</label>
                                                            <select class="form-control">
                                                                <option>1 quarto</option>
                                                                <option>2 quartos</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Adultos</label>
                                                            <select class="form-control">
                                                                <option>0</option>
                                                                <option>1</option>
                                                                <option>2</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Crianças</label>
                                                            <select class="form-control">
                                                                <option>0</option>
                                                                <option>1</option>
                                                                <option>2</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                                
                                                <div class="row hide">                                                                                                        
                                                    <div class="col-md-3 col-md-offset-3 col-sm-3 col-sm-offset-3">
                                                        <div class="form-group">
                                                            <label>Adultos</label>
                                                            <select class="form-control">
                                                                <option>0</option>
                                                                <option>1</option>
                                                                <option>2</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Crianças</label>
                                                            <select class="form-control">
                                                                <option>0</option>
                                                                <option>1</option>
                                                                <option>2</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Idades das crianças</label>
                                                            <select class="form-control">
                                                                <option>0</option>
                                                                <option>1</option>
                                                                <option>2</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>


                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="CiasAereas" id="gol" value="gol"> GOL
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <label class="radio-inline">
                                                          <input type="radio" name="CiasAereas" id="tam" value="tam"> TAM
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <label class="radio-inline">
                                                          <input type="radio" name="CiasAereas" id="azul" value="azul"> AZUL
                                                        </label>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <label class="radio-inline">
                                                          <input type="radio" name="CiasAereas" id="amadeus" value="amadeus"> AMADEUS
                                                        </label>
                                                    </div>
                                               </div>

                                               <div class="row">

                                                    <div class="cotacao">
                                                        <p>Cotação:</p>
                                                        <p>Dolar 2,36</p>
                                                        <p>Euro: 3,18</p>
                                                        <p>Libra: 3,73</p>
                                                    </div>
                                                    
                                                   
                                                   <button type="submit" class="btn btn-default btnyellow">
                                                       <p>Buscar</p>
                                                        <span class="glyphicon glyphicon-search"></span>
                                                   </button>
                                                   

                                               </div>

                                            </form>
                                        </div>
                                        <div class="tab-pane" id="hotel"> 
                                            <?php include 'templates/form-hoteis.php';?>
                                        </div>
                                        <div class="tab-pane" id="pacotesonline"> form pacotes online</div>                      
                                    </div><!-- tab-content -->
                                </div><!-- body -->
                            </div><!-- motor -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- destaquemotor -->   
</div><!-- #home -->    
      
<?php include "footer.php"; ?>