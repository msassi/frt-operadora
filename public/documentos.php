<?php include 'header.php'; ?>            

<div id="documentos" class="resultbusca">

    <div class="head">
        <div class="container">                           
            <div class="bg-azul">
                <span class="cvg pessoas"> </span>
                <h3>Documentos</h3>
            </div>                            
        </div>
    </div><!-- head --> 

    <div class="container">
        <div class="row">         
            <div class="col-md-12 col-sm-4 hidden-xs">
                <div class="text-content">
                    <p>Agente de viagens. Abaixo você encontrará a documentação necessária para seu passageiro.<br \>
                        Clique sobre o documento que deseja para efetuar seu download</p>
                </div>
                <ul>
                    <li><a href="#">
                            <button class="btn btn-default btnyellow">
                                <p>Baixar</p>
                                <span class="cvg cvg-baixar"></span>
                            </button>
                        </a><p class="desc-arquivo">Autorização de Débito</p>
                    </li>
                    <li><a href="#">
                            <button class="btn btn-default btnyellow">
                                <p>Baixar</p>
                                <span class="cvg cvg-baixar"></span>
                            </button>
                        </a><p class="desc-arquivo">Solicitação de Financiamento</p>
                    </li>
                    <li><a href="#">
                            <button class="btn btn-default btnyellow">
                                <p>Baixar</p>
                                <span class="cvg cvg-baixar"></span>
                            </button>
                        </a><p class="desc-arquivo">Autorização para Hospedagem Nacional de menores desacompanhados</p>
                    </li>
                    <li><a href="#">
                            <button class="btn btn-default btnyellow">
                                <p>Baixar</p>
                                <span class="cvg cvg-baixar"></span>
                            </button>
                        </a><p class="desc-arquivo">Contrato de viagens</p>
                    </li>
                    <li><a href="#">
                            <button class="btn btn-default btnyellow">
                                <p>Baixar</p>
                                <span class="cvg cvg-baixar"></span>
                            </button>
                        </a><p class="desc-arquivo">Exigência de Seguro</p>
                    </li>
                    <li><a href="#">
                            <button class="btn btn-default btngreen">
                                <p>Acessar</p>
                                <span class="cvg cvg-acessar"></span>
                            </button>
                        </a><p class="desc-arquivo">Reembolso</p>
                    </li>
                    <li><a href="#">
                            <button class="btn btn-default btngreen">
                                <p>Acessar</p>
                                <span class="cvg cvg-acessar"></span>
                            </button>
                        </a><p class="desc-arquivo">Elogio/ Sugestão e Incidência</p>
                    </li>
                </ul>
            </div>

        </div>
    </div><!-- container -->





</div><!-- .resultbusca -->            

<?php include 'footer.php'; ?>