<?php include 'header.php'; ?>            

<div id="orcamentos" class="resultbusca">

    <div class="head">
        <div class="container">                           
            <div class="bg-azul">
                <span class="glyphicon glyphicon-edit"> </span>
                <h3>Orçamentos</h3>
            </div>                            
        </div>
    </div><!-- head --> 


    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <form class="orcamentos-busca">
                    <div class="fundo-cinsa">
                        <div class="row form-group">
                            <div class="col-md-3"><label >Data de reserva</label>
                                <input type="date" class="form-control"></div>
                            <div class="col-md-3"><label >a</label>
                                <input type="date" class="form-control"></div>
                            <div class="col-md-4"><label >Nome PAX ou localizador</label>
                                <input type="tel" class="form-control"></div>
                            <div class="col-md-2">
                                <button class="btn btn-default btnyellow pull-right" type="button">
                                    <p>Buscar</p>
                                    <span class=" glyphicon glyphicon-search"></span>
                                </button></div>
                        </div>
                    </div>



                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="result-orcamento table table-striped">
                    <thead class="cabecalho-tabela">
                        <tr>
                            <td></td>
                            <td>Operador</td>
                            <td>Usuário</td>
                            <td class="pax">PAX</td>
                            <td>Pago</td>
                            <td>Data</td>
                            <td></td>
                        </tr>
                    </thead>

                    <tr>
                        <td>00000</td>
                        <td>João Carlos</td>
                        <td>Maria Lucia</td>
                        <td>Alvaro Ojeda</td>
                        <td>Não</td>
                        <td>08/10/2014</td>
                        <td> 16:16:16</td>
                    </tr>
                    <tr>
                        <td>00000</td>
                        <td>João Carlos</td>
                        <td>Maria Lucia</td>
                        <td>Alvaro Ojeda</td>
                        <td>Não</td>
                         <td>08/10/2014</td>
                        <td> 16:16:16</td>
                    </tr>
                    <tr>
                         <td>00000</td>
                        <td>João Carlos</td>
                        <td>Maria Lucia</td>
                        <td>Alvaro Ojeda</td>
                        <td>Não</td>
                         <td>08/10/2014</td>
                        <td> 16:16:16</td>
                    </tr>
                    <tr>
                        <td>00000</td>
                        <td>João Carlos</td>
                        <td>Maria Lucia</td>
                        <td>Alvaro Ojeda</td>
                        <td>Não</td>
                        <td>08/10/2014</td>
                        <td> 16:16:16</td>
                    </tr>
                    <tr>
                         <td>00000</td>
                        <td>João Carlos</td>
                        <td>Maria Lucia</td>
                        <td>Alvaro Ojeda</td>
                        <td>Não</td>
                         <td>08/10/2014</td>
                        <td> 16:16:16</td>
                    </tr>

                </table> 
            </div>
        </div>
        <nav class="paginacao-orcamento">
            <ul class="pagination">
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-forward"></span></a></li>
            </ul>
        </nav>
    </div>
</div><!-- container -->


<?php include 'footer.php'; ?>