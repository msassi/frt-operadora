<?php include 'header.php'; ?>            

<div id="roteiros" class="resultbusca">

    <div class="head">
        <div class="container">                           
            <div class="bg-azul">
                <span class="cvg camera"> </span>
                <h3>Sugestões de roteiro</h3>
            </div>                            
        </div>
    </div><!-- head --> 

    <div class="container">
        <div class="row">         
            <div class="col-md-4 col-sm-4 hidden-xs">
                <div class="sidebarfiltro">
                    <div class="boxtitle">
                        <span class="cvg iconlist"></span>
                        <p>Filtre sua pesquisa</p>

                    </div><!-- boxtitle -->
                    <div class="content">
                        <form role="form" class="form-roteiro">
                            <div class="form-group pull-left">
                                <label><strong>Origem</strong></label>
                                <select class="form-control">
                                    <option >Escolha uma origem</option>
                                    <option>Destino 1</option>        
                                    <option>Destino 1</option>        
                                    <option>Destino 1</option>        
                                    <option>Destino 1</option>        
                                </select>
                            </div>
                            <div class="form-group pull-left">
                                <label><strong>Destino</strong></label>
                                <select class="form-control">
                                    <option >Escolha um destino</option>
                                    <option>Destino 1</option>        
                                    <option>Destino 1</option>        
                                    <option>Destino 1</option>        
                                    <option>Destino 1</option>        
                                </select>
                            </div>
                            <div class="form-group pull-left mes">
                                <label><strong>Mês de partida</strong></label>
                                <select class="form-control">
                                    <option >Escolha um mês</option>
                                    <option value="01">Janeiro</option>
                                    <option value="02">Fevereiro</option>
                                    <option value="03">Março</option>
                                    <option value="04">Abril</option>
                                    <option value="05">Maio</option>
                                    <option value="06">Junho</option>
                                    <option value="07">Julho</option>
                                    <option value="08">Agosto</option>
                                    <option value="09">Setembro</option>
                                    <option value="10">Outubro</option>
                                    <option value="11">Novembro</option>
                                    <option value="12">Dezembro</option>        
                                </select>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group pull-left">
                                        <label><strong>Adultos</strong></label>
                                        <select class="form-control">
                                            <option >1</option>
                                            <option >2</option>
                                            <option >3</option>
                                            <option >4</option>
                                            <option >5</option>
                                            <option >6</option>
                                            <option >7</option>
                                            <option >8</option>
                                            <option >9</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group pull-left">
                                        <label><strong>Crianças</strong></label>
                                        <select class="form-control">
                                            <option >0</option>
                                            <option >1</option>
                                            <option >2</option>
                                            <option >3</option>
                                            <option >4</option>
                                            <option >5</option>
                                            <option >6</option>
                                            <option >7</option>
                                            <option >8</option>
                                            <option >9</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-default btnyellow pull-right">
                                <p>Buscar</p>
                                <span class="glyphicon glyphicon-search"></span>
                            </button>

                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-12">
                
                <div class="row roteiro-single">
                    <div class="col-md-12">
                        <img src="img/roteiro1.jpg" class="img-responsive">
                        <div class="bg-azul transparent">
                            <h3>Nome do destino</h3>
                        </div>
                        <div class="col-md-3 col-md-offset-9 confira-rosa">
                            <a href="#"><span class="cvg camera-rosa"></span> Confira esse pacote</a>
                        </div>
                    </div>
                </div>
                
                <div class="row roteiro-single">
                    <div class="col-md-12">
                        <img src="img/roteiro1.jpg" class="img-responsive">
                        <div class="bg-azul transparent">
                            <h3>Nome do destino</h3>
                        </div>
                        <div class="col-md-3 col-md-offset-9 confira-rosa">
                            <a href="#"><span class="cvg camera-rosa"></span> Confira esse pacote</a>
                        </div>
                    </div>
                </div>
                
                <div class="row roteiro-single">
                    <div class="col-md-12">
                        <img src="img/roteiro1.jpg" class="img-responsive">
                        <div class="bg-azul transparent">
                            <h3>Nome do destino</h3>
                        </div>
                        <div class="col-md-3 col-md-offset-9 confira-rosa">
                            <a href="#"><span class="cvg camera-rosa"></span> Confira esse pacote</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div><!-- container -->





</div><!-- .resultbusca -->            

<?php include 'footer.php'; ?>