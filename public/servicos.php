<?php include 'header.php'; ?>            

<div id="servicos" class="resultbusca">
    <div class="head">
        <div class="container">
            <div class="row">         

                <div class="col-md-9 col-md-offset-3 col-sm-8 col-xs-12">
                    <ul class="menuresult">
                        <li>                            
                            <span class="cvg menuresultcvg aereo hidden-xs"></span>
                            <p>Aéreo</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li class="active">                            
                            <span class="cvg menuresultcvg hotel hidden-xs "></span>
                            <p>Hotel</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li>
                            <span class="cvg menuresultcvg servicos hidden-xs active"></span>
                            <p>Serviços</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li>                            
                            <span class="cvg menuresultcvg orcamento hidden-xs"></span>
                            <p>Orçamento</p>                                                            
                        </li>                        
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- head --> 

    <div class="container">
        <div class="row row-offcanvas row-offcanvas-left">


            <div class="col-md-9 col-md-offset-3 col-sm-8 col-xs-12">

                <p class="pull-left togglefiltro visible-xs">
                    <button type="button" data-toggle="offcanvas">
                        <span class="glyphicon glyphicon-filter"></span>
                        Filtre sua pesquisa
                    </button>
                </p>

                <div class="clearfix"></div>

                <div class="titulo">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="head ">
                                <h3>Escolher <strong>outros serviços</strong></h3>

                            </div>
                        </div>
                        <div class="col-md-12">

                            <div class="body">
                                <form class="form-inline" role="form">
                                    <div class="row titulo-azul">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Ofertas receptivos</label>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Moeda</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Total</label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label></label>
                                            </div>
                                        </div>

                                    </div>  
                                    <hr class="boder-dotted">
                                    <div class="row ">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"> Bolsa FR
                                                    </label>
                                                </div>                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label> BRL </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label> 54,06 </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="date" class="form-control"  placeholder="dd/mm/aa">
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="boder-dotted">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"> Bolsa FR
                                                    </label>
                                                </div>                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label> BRL </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label> 54,06 </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="date" class="form-control"  placeholder="dd/mm/aa">
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="boder-dotted">
                                    <div class="row ">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"> Bolsa FR
                                                    </label>
                                                </div>                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label> BRL </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label> 54,06 </label>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <input type="date" class="form-control"  placeholder="dd/mm/aa">
                                            </div>
                                        </div>
                                    </div>

                                    <hr class="boder-dotted">
                                    <div class="row ">

                                        <div class="col-md-2 col-md-offset-10">
                                            <button class="btn btn-default btnred">
                                                <p>Continuar</p>
                                                <span class="glyphicon glyphicon-play"></span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div><!-- row -->
    </div><!-- container -->





</div><!-- .resultbusca -->            

<?php include 'footer.php'; ?>