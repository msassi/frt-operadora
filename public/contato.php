<?php include 'header.php'; ?>            

<div id="contato" class="resultbusca">

    <div class="head">
        <div class="container">                           
            <div class="bg-azul">
                <span class="cvg contato"> </span>
                <h3>Contatos</h3>
            </div>                            
        </div>
    </div><!-- head --> 

    <div class="container">
        <div class="row lojas">    
            <div class="col-md-12">
                <h2>Lojas</h2>

                <div class="panel with-nav-tabs panel-default">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#tab1default" data-toggle="tab">Foz do iguaçu</a>
                                <span class="cvg seta-azul-clara"></span></li>
                            <li><a href="#tab2default" data-toggle="tab">Curitiba</a><span class="cvg seta-azul-clara"></span></li>
                            <li><a href="#tab3default" data-toggle="tab">Londrina</a><span class="cvg seta-azul-clara"></span></li>
                            <li><a href="#tab4default" data-toggle="tab">Maringa</a><span class="cvg seta-azul-clara"></span></li>
                            <li><a href="#tab5default" data-toggle="tab">Porto Alegre</a><span class="cvg seta-azul-clara"></span></li>
                            <li><a href="#tab6default" data-toggle="tab">Bauru</a><span class="cvg seta-azul-clara"></span></li>

                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1default">
                                <table class="fundo-azul">
                                    <thead class="cabecalho-tabela">
                                        <tr>
                                            <td>Av. Brasil, 720- Sala 101- Centro</td>
                                            <td>CEP: 85851-000</td>
                                            <td>Tel: <b>+55(45)3521-8500</b></td>
                                            <td><a href="#">Mapa: Ver localização</a></td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td>Ana Paula Lara</td>
                                        <td>vendasigu1@frt.tur.br</td>
                                        <td>anapaulalara2@hotmail.com</td>
                                        <td>(45)3521-8515</td>
                                    </tr>
                                    <tr>
                                        <td> Cleo Matias</td>
                                        <td>vendasigu2@frt.tur.br</td>
                                        <td>cleo_frt@hotmail.com</td>
                                        <td>(45)3521-8512</td>
                                    </tr>
                                    <tr>
                                        <td>Kelly Almeida</td>
                                        <td>vendasigu8@frt.tur.br</td>
                                        <td>almeidakma@hotmail.com</td>
                                        <td>(45)3521-8508</td>
                                    </tr>
                                    <tr>
                                        <td>Jane Barzi</td>
                                        <td>vendasigu5@frt.tur.br</td>
                                        <td>janemeire26@hotmail.com</td>
                                        <td>(45)3521-8503</td>
                                    </tr>
                                    <tr>
                                        <td>Paula Feniman</td>
                                        <td>vendasigu6@frt.tur.br</td>
                                        <td>paula_klivia@hotmail.com</td>
                                        <td> (45)3521-8507</td>
                                    </tr>
                                    <tr>
                                        <td>Silvana Ceriolli Ojeda</td>
                                        <td>vendasigu7@frt.tur.br</td>
                                        <td>silvana-frt@hotmail.com</td>
                                        <td>(45)3521-8562</td>
                                    </tr>
                                    <tr>
                                        <td>Thais Bueno</td>
                                        <td>vendasigu9@frt.tur.br</td>
                                        <td>thais_frt@hotmail.com</td>
                                        <td>(45)3521-8504</td>
                                    </tr>
                                    <tr>
                                        <td> Fernanda Ricthie</td>
                                        <td>vendasigu4@frt.tur.br</td>
                                        <td>fericthie_frt@hotmail.com</td>
                                        <td>(45)3521-8518</td>
                                    </tr>
                                    <tr>
                                        <td>Edna Moreira</td>
                                        <td>vendasigu3@frt.tur.br</td>
                                        <td>ednamoreira62@hotmail.com</td>
                                        <td>(45)3521-8541</td>
                                    </tr>

                                </table>

                            </div>
                            <div class="tab-pane fade" id="tab2default">
                                <table class="fundo-azul">
                                    <thead class="cabecalho-tabela">
                                        <tr>
                                            <td>Av. Visconde do Rio Branco, 1358- Loja 06-Centro</td>
                                            <td>CEP: 80420-210</td>
                                            <td>Tel: <b>+55(41)3232-0101</b></td>
                                            <td><a href="#">Mapa: Ver localização</a></td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td>Cleison</td>
                                        <td>vendascwb3@frt.tur.br</td>
                                        <td>cleisondenis</td>
                                        <td>(41)3232-0101</td>
                                    </tr>
                                    <tr>
                                        <td>Cláudia</td>
                                        <td>vendascwb6@frt.tur.br</td>
                                        <td> claudiawbraga85</td>
                                        <td>(41)3232-0101</td>
                                    </tr>
                                    <tr>
                                        <td>Dione Gouveia</td>
                                        <td>vendascwb4@frt.tur.br</td>
                                        <td>doine-gouvea</td>
                                        <td>(41)3232-0101</td>
                                    </tr>
                                    <tr>
                                        <td>Cintia Campitelli</td>
                                        <td>vendascwb5@frt.tur.br</td>
                                        <td>ci_cozer</td>
                                        <td>(41)3232-0101</td>
                                    </tr>
                                    <tr>
                                        <td>Diego Amaral</td>
                                        <td>vendascwb2@frt.tur.br</td>
                                        <td>diego_amaral6@hotmail.com</td>
                                        <td>(41)3232-0101</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="tab3default">
                                <table class="fundo-azul">
                                    <thead class="cabecalho-tabela">
                                        <tr>
                                            <td>Rua Pará, 1500- Sala 402- Centro</td>
                                            <td>CEP: 86020-400</td>
                                            <td>Tel: <b>+55(43)3345-1030</b></td>
                                            <td><a href="#">Mapa: Ver localização</a></td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td> Ana Claudia </td>
                                        <td>vendasldb3@frt.tur.br</td>
                                        <td>vendasldb3@frt.tur.br</td>
                                        <td>(43)3345-1030</td>
                                    </tr>
                                    <tr>
                                        <td> Elaine Prado</td>
                                        <td>vendasldb1@frt.tur.br</td>
                                        <td> elaine@frt.tur.br</td>
                                        <td>(43)3345-1030</td>
                                    </tr>
                                    <tr>
                                        <td>Cleber Bento </td>
                                        <td>vendasldb2@frt.tur.br</td>
                                        <td>cleber@frt.tur.br</td>
                                        <td>(43)3345-1030</td>
                                    </tr>
                                    <tr>
                                        <td>Aline Santos</td>
                                        <td>vendasldb5@frt.tur.br</td>
                                        <td>aline.frt_@hotmail.com</td>
                                        <td>(43)3345-1030</td>
                                    </tr>
                                    <tr>
                                        <td>Karolyne Prandi</td>
                                        <td>vendasldb4@frt.tur.br</td>
                                        <td>karolprandi.frt@hotmail.com</td>
                                        <td>(43)3345-1030</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="tab4default">
                                <table class="fundo-azul">
                                    <thead class="cabecalho-tabela">
                                        <tr>
                                            <td>Av. Adv. Horácio Raccanello Filho, 5570, Sala 1104- Centro</td>
                                            <td>CEP: 87020-035</td>
                                            <td>Tel: <b>+55(44)3023-0940</b></td>
                                            <td><a href="#">Mapa: Ver localização</a></td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td>Grasielle Leme Pinheiro </td>
                                        <td>vendasmgf5@frt.tur.br</td>
                                        <td></td>
                                        <td>(44)3023-0940</td>
                                    </tr>
                                    <tr>
                                        <td>Raquel Sanches </td>
                                        <td> vendasmgf1@frt.tur.br</td>
                                        <td>raquel@frt.tur.br</td>
                                        <td>(44)3023-0940</td>
                                    </tr>
                                    <tr>
                                        <td>Augusto Bozeli </td>
                                        <td>vendasmgf2@frt.tur.br</td>
                                        <td>augustomgf@hotmail.com</td>
                                        <td>(44)3023-0940</td>
                                    </tr>
                                    <tr>
                                        <td>Sara Isabel Carmona </td>
                                        <td>vendasmgf4@frt.tur.br</td>
                                        <td>saraalvarezcarmona</td>
                                        <td> (44)3023-0940</td>
                                    </tr>
                                </table> 

                            </div>
                            <div class="tab-pane fade" id="tab5default">
                                <table class="fundo-azul">
                                    <thead class="cabecalho-tabela">
                                        <tr>
                                            <td>Av. Borges de Medeiros, 328- Sala 161</td>
                                            <td>CEP: 90020-020</td>
                                            <td>Tel: <b>+55(51)3061-7961</b></td>
                                            <td><a href="#">Mapa: Ver localização</a></td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td>Dayane Ribeiro</td>
                                        <td>vendaspoa2@frt.tur.br</td>
                                        <td>vendaspoa2_2</td>
                                        <td>(51)3061-7961</td>
                                    </tr>
                                    <tr>
                                        <td>Graziele Gonçalves</td>
                                        <td>vendaspoa1@frt.tur.br</td>
                                        <td>vendaspoa1_2</td>
                                        <td>(51)3061-7961</td>
                                    </tr>
                                </table>

                            </div>

                            <div class="tab-pane fade" id="tab6default">
                                <table class="fundo-azul">
                                    <thead class="cabecalho-tabela">
                                        <tr>
                                            <td> Av. Borges de Medeiros, 328- Sala 161</td>
                                            <td>Tel: <b>0800-605-1030/0800-605-0940</b></td>
                                            <td><a href="#">Mapa: Ver localização</a></td>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td>Vanderlei Melo</td>
                                        <td>comercialsao@frt.tur.br</td>
                                        <td>(14)98187-5774</td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h2>Produtos/ MKT/ TI</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="fundo-azul">
                    <thead class="cabecalho-tabela">
                        <tr>
                            <td>Nome</td>
                            <td>E-mail</td>
                            <td>Setor</td>
                            <td>Telefone</td>
                        </tr>
                    </thead>
                    <tr>
                        <td>Ariva neves</td>
                        <td>mkt@frt.tur.br</td>
                        <td>Gerente de Marketing</td>
                        <td>(45)3521-8517</td>
                    </tr>
                    <tr>
                        <td>Carmem Borges</td>
                        <td> ti@frt.tur.br</td>
                        <td>Gerente de TI</td>
                        <td>(45)3521-8525</td>
                    </tr>
                    <tr>
                        <td>Fábio Silva</td>
                        <td>gerenciadeprodutos@frt.tur.br</td>
                        <td>Gerente de Produtos</td>
                        <td>(45)3521-8517</td>
                    </tr>
                    <tr>
                        <td>Tarifas de Hotel/Serviços</td>
                        <td>hoteis@frt.tur.br</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td> Wilson</td>
                        <td>supervisordeprodutos@frt.tur.br</td>
                        <td>Supervisor de produtos</td>
                        <td> (45)3521-8517</td>
                    </tr>
                </table> 
            </div>

        </div>

        <div class="row ">
            <div class="col-md-12">
                <h2>Gerentes de vendas</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="fundo-azul">
                    <thead class="cabecalho-tabela">
                        <tr>
                            <td>Nome</td>
                            <td>E-mail</td>
                            <td>Setor</td>
                            <td>Telefone</td>
                        </tr>
                    </thead>
                    <tr>
                        <td>Rogério Xavier</td>
                        <td>gerenciapoa@frt.tur.br</td>
                        <td>Gerente de Vendas-POA</td>
                        <td>(51)3092-6400</td>
                    </tr>
                    <tr>
                        <td>Elenice Alvim</td>
                        <td>gerenciaigu@frt.tur.br</td>
                        <td>Gerente de Vendas-IGU</td>
                        <td>(45)3521-8502</td>
                    </tr>
                    <tr>
                        <td>Diego Soares</td>
                        <td>gerencialdb@frt.tur.br</td>
                        <td>Gerente de Vendas-LDB</td>
                        <td>(43)3345-1030</td>
                    </tr>

                    <tr>
                        <td>Edson Gomes</td>
                        <td>gerenciamgf@frt.tur.br</td>
                        <td>Gerente de Vendas-MGF</td>
                        <td>(44)3023-0940</td>
                    </tr>
                    <tr>
                        <td>Dayana Medes</td>
                        <td>gerenciacwb@frt.tur.br</td>
                        <td>Gerente de Vendas-CWB</td>
                        <td>(41)3223-0101</td>
                    </tr>

                </table>


            </div>
        </div>

        <div class="row ">
            <div class="col-md-12">
                <h2>Diretores</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="fundo-azul">
                    <thead class="cabecalho-tabela">
                        <tr>
                            <td>Nome</td>
                            <td>E-mail</td>
                            <td>Setor</td>
                            <td>Telefone</td>
                        </tr>
                    </thead>
                    <tr>
                        <td>Danielle Morales Meireles</td>
                        <td>diretoriaigu@frt.tur.br</td>
                        <td>Diretoria Geral</td>
                        <td>(45)3521-8500</td>
                    </tr>
                    <tr>
                        <td>Giovanni Barzi</td>
                        <td>diretoria@frt.tur.br</td>
                        <td>Diretoria</td>
                        <td>(45)3521-8500</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12">
                <h2>Comercial</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="fundo-azul">
                    <thead class="cabecalho-tabela">
                        <tr>
                            <td>Nome</td>
                            <td>E-mail</td>
                            <td>Setor</td>
                            <td>Telefone</td>
                        </tr>
                    </thead>
                    <tr>
                        <td>Madsom Silva</td>
                        <td>comercialigu@frt.tur.br</td>
                        <td>Comercial IGU</td>
                        <td>(45)3521-8500</td>
                    </tr>
                    <tr>
                        <td>Thiago Gomes</td>
                        <td>comercialcwb@frt.tur.br</td>
                        <td>Comercial CWB</td>
                        <td>(41)3232-0101</td>
                    </tr>
                    <tr>
                        <td>Vanderlei Melo</td>
                        <td>comercialsao@frt.tur.br</td>
                        <td>Comercial LDB/MGF</td>
                        <td>(43)3345-1030</td>
                    </tr>
                    <tr>
                        <td>André</td>
                        <td>comercialldbmgf@frt.tur.br</td>
                        <td>Comercial LDB/MGF</td>
                        <td>(43)8802-0065</td>
                    </tr>
                    <tr>
                        <td>Alexandra</td>
                        <td>comercialpoaf@frt.tur.br</td>
                        <td>Comercial POA</td>
                        <td>(51)3061-7691</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12">
                <h2>Departamento Financeiro</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="fundo-azul">
                    <thead class="cabecalho-tabela">
                        <tr>
                            <td>Nome</td>
                            <td>E-mail</td>
                            <td>Setor</td>
                            <td>Telefone</td>
                        </tr>
                    </thead>
                    <tr>
                        <td>Paulo</td>
                        <td>financeiroldb@frt.tur.br</td>
                        <td>Financeiro LDB/MGF</td>
                        <td>(43)3345-1030</td>
                    </tr>
                    <tr>
                        <td>Maria Santos</td>
                        <td>contasareceber@frt.tur.br</td>
                        <td>Contas a receber</td>
                        <td>Contas a receber</td>
                    </tr>
                    <tr>
                        <td>Amábile Chagas</td>
                        <td>ciasaereas@frt.tur.br</td>
                        <td>Cias Aéreas/ Reembolso</td>
                        <td>(45)3521-8520</td>
                    </tr>
                    <tr>
                        <td>Viviane Santos</td>
                        <td>contasapagar@frt.tur.br</td>
                        <td>Contas a pagar</td>
                        <td>(45)3521-8519</td>
                    </tr>
                    <tr>
                        <td>Camila Frasson</td>
                        <td>conferenciacartao@frt.tur.br</td>
                        <td>Administrativo</td>
                        <td>(45)3521-8528</td>
                    </tr>
                    <tr>
                        <td>Andressa Pinheiro</td>
                        <td>financeiroigu@frt.tur.br</td>
                        <td>Gerente Financeiro</td>
                        <td>(45)3521-8528</td>
                    </tr>
                    <tr>
                        <td>Katielle</td>
                        <td>internacionais@frt.tur.br</td>
                        <td>Internacionais</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Camila Pujol</td>
                        <td>administrativopoa@frt.tur.br</td>
                        <td>Administrativo</td>
                        <td>(51)3061-7691</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row ">
            <div class="col-md-12">
                <h2>Deixe sua mensagem</h2>
            </div>
        </div>




        <form class="contato-formulario">
            <div class="fundo-cinsa">
                <div class="row form-group ">
                    <div class="col-md-3"><label >Nome</label>
                        <input type="text" class="form-control"></div>
                    <div class="col-md-3"><label >Email</label>
                        <input type="email" class="form-control"></div>
                    <div class="col-md-3"><label >Telefone</label>
                        <input type="tel" class="form-control"></div>
                    <div class="col-md-3"><label >Assundo</label>
                        <input type="text" class="form-control"></div>
                </div>
            </div>
            <div class="row form-group areadetexto">
                <div class="col-md-12">
                    <textarea  class="form-control" rows="6"></textarea>
                </div>
            </div>
            <div class=" form-group">
                <button class="btn btn-default btn-azul pull-right" type="submit">
                    <p>Enviar</p>
                    <span class=" glyphicon glyphicon-send"></span>
                </button></div>
        </form>
    </div>
</div><!-- container -->

</div>
</div>           

<?php include 'footer.php'; ?>