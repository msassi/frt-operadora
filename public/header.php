<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FRT Operadora</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="jqueryui/jquery-ui.css" rel="stylesheet">
    <link href="css/style.css?v=2" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
<body>            
<div id="topo"></div>      
<header id="header">  
    <div class="container">
        <div class="row">
              <div class="col-md-4 col-sm-4">                  
                  <div class="logo">
                      <a href="index.php"><img src="img/logo_frt_operadora.png" class="img-responsive"></a>
                  </div>
              </div>
            
            <div class="col-md-8 col-sm-8">
                <div class="navbar navbar-inverse">

                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav menu">
                                <li>
                                    <a href="aereo.php">
                                        <span class="cvg iconsmenu home hidden-xs active"></span>
                                        <p>Início</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="cvg iconsmenu roteiros hidden-xs active"></span>
                                        <p>Sugestões de Roteiros</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="cvg iconsmenu cadastroag hidden-xs active"></span>
                                        <p>Cadastre sua agenda</p>
                                        <!-- quando o cara tiver logado, aqui aparece minha agencia-->
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="cvg iconsmenu docs hidden-xs active"></span>
                                        <p>Documentos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="cvg iconsmenu icoquemsomos hidden-xs active"></span>
                                        <p>Quem somos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="cvg iconsmenu contatos hidden-xs active"></span>
                                        <p>Contatos</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="cvg iconsmenu bancolaminas hidden-xs active"></span>
                                        <p>Banco de Laminas</p>
                                    </a>
                                </li>                                      
                            </ul>                                        
                        </div><!--/.nav-collapse -->

                </div><!-- navbar -->                  
            </div>
        </div>
    </div><!-- .container -->      
    
    <div class="login">
        <div class="container">
            <div class="row">

                <!-- // QUANDO NÂO TIVE LOGADO -->
                <!--
                <div class="col-md-7 col-sm-10 col-xs-12 pull-right">
                    <div class="logar">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                              <input type="email" class="form-control" id="email" placeholder="Login">
                            </div>
                            <div class="form-group">
                              <input type="password" class="form-control" id="senha" placeholder="Senha">
                            </div>

                                <button type="submit" class="btn-default">
                                    <span class="cvg chevron-right-submit"></span>
                                </button>

                        </form>
                    </div>
                </div>
                -->
                <!-- //fim login -->

                <!-- // JA LOGADO -->
                <div class="col-md-12 col-sm-12 col-xs-12 pull-right">
                    <div class="logado">
                        <form class="form-inline" role="form">
                            <div class="form-group">                      
                              <input type="text" class="form-control" placeholder="FRT Operadora">
                            </div>                            
                            <div class="form-group name">                      
                               <label>Jose Silverio da Silva</label>
                            </div>    
                            <div class="form-group"> 
                                <button type="submit" class="btn-default">
                                    Sair
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- // FIM JALOGADO -->

            </div>
        </div>    
    </div><!-- login -->
</header>


