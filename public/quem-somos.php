<?php include 'header.php'; ?>            

<div id="quem-somos" class="resultbusca">
    
    <div class="head">
        <div class="container">                           
            <div class="bg-azul">
                 <span class="cvg pessoas"> </span>
                 <h3>Quem somos</h3>
            </div>                            
        </div>
    </div><!-- head --> 

    <div class="container">
        <div class="row">         
            <div class="col-md-12 col-sm-4 hidden-xs">
                <div class="text-content">
                    <p>A <strong>FRT Operadora</strong> de turismo nasceu em Foz do Iguaçu no ano de 2001, a partir de um pequeno departamento operacional do Grupo FRONTUR, que atua com sucesso, ética e seriedade no mercado turístico desde 1979. Hoje somos uma operadora de viagens de negócios, lazer e eventos e nosso objetivo é a excelência no atendimento de suas necessidades, atuando nos mercados nacionais e internacionais.</p>
                    <p>O <strong>Portal do Agente</strong> é nossa base online para vendas e permite sua total independência, agente de viagens, quanto a reserva e emissão de passagens aéreas, hospedagem e serviços. Você pode efetuar seu cadastro aqui. Seguimos com destaque pelo alto investimento em treinamento da nossa equipe, em tecnologia de reservas on-line e de suporte comercial.</p>
                    Dentre os principais produtos e serviços que prestamos, destacamos:
                    <ul>
                        <li>Passagens aéreas nacionais e internacionais;</li>
                        <li>Reservas de hotéis nacionais e internacionais;</li>
                        <li>Pacotes de viagens nacionais e internacionais;</li>
                        <li>Circuitos e roteiros por todos os continentes;</li>
                        <li>Seguros de viagem;</li>
                        <li>Assistência a Vistos Consulares;</li>
                        <li>Oferecemos as melhores companhias aéreas, hoteleiras e de serviços no mundo para você, agente de viagens.</li>
                    </ul>
                    Somos FRT Operadora de Turismo
                    
                    <h3><strong>Todo mundo está aqui!</strong></h3>
                </div>
            </div>
           
        </div>
    </div><!-- container -->





</div><!-- .resultbusca -->            

<?php include 'footer.php'; ?>