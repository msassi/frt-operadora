<?php include 'header.php';?>

<div id="hotel" data-enable="1"  class="resultbusca">
    <div class="head">
        <div class="container">
            <div class="row">         
                <div class="col-md-3 col-sm-4 hidden-xs">
                    <div class="sidebarfiltro">
                        <div class="boxtitle">
                            <span class="cvg iconlist"></span>
                            <p>Filtre sua pesquisa</p>
                            <span class="cvg setaazul"></span>
                        </div><!-- boxtitle -->
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <ul class="menuresult">
                        <li>                            
                            <span class="cvg menuresultcvg aereo hidden-xs"></span>
                            <p>Aéreo</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li class="active">                            
                            <span class="cvg menuresultcvg hotel hidden-xs active"></span>
                            <p>Hotel</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li>
                            <span class="cvg menuresultcvg servicos hidden-xs"></span>
                            <p>Serviços</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li>                            
                            <span class="cvg menuresultcvg orcamento hidden-xs"></span>
                            <p>Orçamento</p>                                                            
                        </li>                        
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- head --> 
    
    <div class="container">
        <div class="row row-offcanvas row-offcanvas-left">
                        
            <div class="col-md-3 col-sm-4 col-xs-6 sidebar-offcanvas" id="sidebar" role="navigation">
                <div class="sidebarfiltro">                    
                    <div class="content sidebar-fix-top">
                        <form role="form">
                            <div class="form-group pull-left">
                                <label><strong>Ordenar por</strong></label>
                                <select class="form-control">
                                    <option>Preço menor a maior</option>        
                                </select>
                            </div>
                            <div class="form-group pull-left">
                                <label><strong>Procure pelo nome do Hotel</strong></label>
                                <input type="text" class="form-control" id="hotel" placeholder="Digite o nome do Hotel">
                            </div>
                            <div class="form-group pull-left">                              
                              <div class="sliderjq">
                                    <div class="bgslider"></div>
                                    <label for="amount"><strong>Preço</strong></label>
                                    <input type="text" id="amount" readonly>
                              </div>
 
                              <div id="slider-range"></div>
                              
                            </div>
                            <div class="form-group pull-left">
                                <label><strong>Classificação</strong></label> 
                                <div class="stars">
                                    <span class="cvg starlarge star"></span>
                                    <span class="cvg starlarge star"></span>
                                    <span class="cvg starlarge star"></span>
                                    <span class="cvg starlarge star"></span>
                                    <span class="cvg starlarge starempty"></span>
                                </div>                                
                            </div>
                            <div class="form-group pull-left">
                                <label><strong>Tipos de Pensões</strong></label>
                                <select class="form-control">
                                    <option>Todas</option>        
                                </select>
                            </div>
                        </form>
                    </div><!-- content -->
                </div><!-- sidebarfiltro -->
            </div>
            
            <div id="list-hotel" class="col-md-9 col-sm-8 col-xs-12">
                
                <p class="pull-left togglefiltro visible-xs">
                    <button type="button" data-toggle="offcanvas">
                        <span class="glyphicon glyphicon-filter"></span>
                        Filtre sua pesquisa
                    </button>
                </p>
                
                <div class="clearfix"></div>
                
                <div class="comparar">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="head">
                                <h3>Compare preços dos hotéis selecionados</h3>
                                
                                <button class="btn btn-default">
                                    <p>Comparar/Mapa</p>
                                    <span class="cvg iconmapa"></span>
                                </button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            
                            <div class="body">
                                <form class="form-inline" role="form">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><strong>Digite o nome da cidade</strong></label>
                                                <input type="text" class="form-control" id="origem" placeholder="Digite o nome">
                                            </div>
                                        </div>
                                    </div>                                    
                                    <div class="row">
                                        <div class="col-md-5 col-sm-6">
                                            <div class="form-group">
                                                <label><strong>Entrada</strong></label>
                                                <input type="date" class="form-control" id="dataIda" placeholder="dd/mm/aa">
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-sm-6">
                                            <div class="form-group">
                                                <label><strong>Saída</strong></label>
                                                <input type="date" class="form-control" id="dataIda" placeholder="dd/mm/aa">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-default btnyellow">
                                                <p>Buscar</p>
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-10">
                                            <label class="namecity">Cidade Porto de Galinhas - Pernambuco (REC) de 00/00/00</label>
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-default btnred">
                                                <p>Continuar</p>
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                       </div>
                        
                    </div>
                </div>
                <div class="resultado">                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            
                            <ul class="item">
                                <?php for ($index = 0; $index < 8; $index++) : ?>
                                <li>
                                    
                                    <div class="pull-left imagem">
                                        <img src="img/imghotel.jpg" class="img-responsive">
                                    </div>
                                    <div class="pull-left info">
                                        <h2>Pontal do Ocaporã</h2>
                                        
                                        <p>Sítio Costa Tropicana</p>
                                        <a href="#">Ver no mapa</a>
                                        
                                        <div class="classificacao">
                                            <p class="green">On line</p>
                                            
                                            <span class="cvg starsmall star"></span>
                                            <span class="cvg starsmall starempty"></span>
                                            <span class="cvg starsmall starempty"></span>
                                            <span class="cvg starsmall starempty"></span>
                                            <span class="cvg starsmall starempty"></span>
                                        </div>
                                        
                                        <div class="text-content">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt, mollitia, obcaecati sed adipisci blanditiis eum assumenda expedita animi reiciendis quibusdam! Temporibus...</p>
                                        </div>
                                    </div>
                                    <div class="pull-right total">
                                        
                                            <h3>À partir de</h3>

                                            <p class="price">
                                                R$<strong> 4.919,00</strong> 
                                            </p>
                                            
                                            <a href="#">
                                                <p class="obs">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                    Opções de Quarto
                                                </p>
                                            </a>

                                            <button type="submit" class="btn btn-default btnred">
                                                <p>Selecionar</p>
                                                <span class="glyphicon glyphicon-pushpin"></span>
                                            </button>
                                        
                                    </div>
                                                                                                                                                                                                                     
                                </li>   
                                
                                 <?php endfor; ?>
                                
                                
                            </ul>                            
                                                        
                        </div>
                        
                        
                    </div>
                    
                </div><!-- resultado -->
            </div>
            
        </div><!-- row -->
    </div><!-- container -->
    
    
    
    
    
</div><!-- .resultbusca -->            

<?php include 'footer.php';?>