<?php include 'header.php';?>            
      


<div id="fix-top" data-enable="1" class="resultbusca">
    <div class="head">
        <div class="container">
            <div class="row">         
                <div class="col-md-3 col-sm-4 hidden-xs">
                    <div class="sidebarfiltro">
                        <div class="boxtitle">
                            <span class="cvg iconlist"></span>
                            <p>Filtre sua pesquisa</p>
                            <span class="cvg setaazul"></span>
                        </div><!-- boxtitle -->
                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <ul class="menuresult">
                        <li class="active">                            
                            <span class="cvg menuresultcvg aereo hidden-xs active"></span>
                            <p>Aéreo</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li>                            
                            <span class="cvg menuresultcvg hotel hidden-xs"></span>
                            <p>Hotel</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li>
                            <span class="cvg menuresultcvg servicos hidden-xs"></span>
                            <p>Serviços</p>                                                            
                        </li>
                        <li class="hidden-xs"><span class="cvg menuresultcvg setapont"></span></li>
                        <li>                            
                            <span class="cvg menuresultcvg orcamento hidden-xs"></span>
                            <p>Orçamento</p>                                                            
                        </li>                        
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- head --> 
    
    <div class="container">
        <div class="row row-offcanvas row-offcanvas-left">
                        
            <div class="col-md-3 col-sm-4 col-xs-6 sidebar-offcanvas" id="sidebar" role="navigation">
                <div class="sidebarfiltro">                    
                    <div class="content sidebar-fix-top">
                        <form role="form">
                            <div class="form-group pull-left">
                              <label>Ordenar por</label>
                              <select class="form-control">
                                  <option>Preço menor a maior</option>        
                              </select>
                            </div>

                            <div class="form-group pull-left">                              
                              <div class="sliderjq">
                                    <div class="bgslider"></div>
                                    <label for="amount">Preço</label>
                                    <input type="text" id="amount" readonly>
                              </div>
                              <div id="slider-range"></div>
                            </div>

                            <div class="form-group pull-left">
                              <div class="sliderjq">
                                    <div class="bgslider"></div>
                                    <label for="amount">Horário de embarque ida</label>
                                    <input type="text" id="time" readonly>
                              </div>
                              <div id="slider-range-horario"></div>
                            </div>

                            <div class="form-group pull-left">
                              <label>Horário de embarque volta</label>
                              <label><strong>00h00 às 23h00</strong></label>    
                            </div>
                            <div class="form-group pull-left">
                              <label>Cias Aéreas</label>
                                <div class="radio">
                                    <label>
                                      <input type="radio" name="cias" id="todasCias" value="todasCias" checked> Todas as Cias. </label>
                                </div>
                                <div class="radio">
                                    <label>
                                      <input type="radio" name="cias" id="gol" value="gol">Gol
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                      <input type="radio" name="cias" id="tam" value="tam">Tam
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                      <input type="radio" name="cias" id="azul" value="azul">Azul
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div><!-- content -->
                </div><!-- sidebarfiltro -->
            </div>
            
            <div id="list-aereo" class="col-md-9 col-sm-8 col-xs-12">
                
                <p class="pull-left togglefiltro visible-xs">
                    <button type="button" data-toggle="offcanvas">
                        <span class="glyphicon glyphicon-filter"></span>
                        Filtre sua pesquisa
                    </button>
                </p>
                
                <div class="clearfix"></div>
                
                <div class="resultado">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" id="tabmotor">
                        <li class="active"><a href="#areo" data-toggle="tab"></span>Aéreo 10</a></li>
                        <li><a href="#bloqueio" data-toggle="tab">Bloqueio 10</a></li>                                      
                    </ul>  
                    
                    <div class="tab-content">
                        <div class="tab-pane active" id="areo">
                            <p>7 voos de Foz do Iguaçu - Paraná (IGU) a Bauru - São Paulo (ITC)</p>
                        </div>
                        <div class="tab-pane" id="bloqueio"> ... </div>
                    </div>
                    
                    <div class="row">

                        <?php for ($index = 0; $index < 3; $index++) : ?>
                        <div class="col-md-12">
                            <ul class="item item-aereo">
                                <li>                                   
                                    <div class="pull-left logo-cia-aerea">
                                        <img src="img/logo-cia-aerea-gol.jpg" class="img-responsive">
                                    </div>
                                    <div class="pull-left info clear">
                                        <p>IGU - Foz do Iguaçu</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>CWB - Curitiba</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Base T</p>
                                        <p>Dur: 00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Escalas 0</p>
                                    </div>                                    
                                </li>
                                <li>
                                    <div class="pull-left logo-cia-aerea">
                                        <img src="img/logo-cia-aerea-gol.jpg" class="img-responsive">
                                    </div>
                                    <div class="pull-left info clear">
                                        <p>IGU - Foz do Iguaçu</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>CWB - Curitiba</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Base T</p>
                                        <p>Dur: 00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Escalas 0</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="pull-left logo-cia-aerea">
                                        <img src="img/logo-cia-aerea-gol.jpg" class="img-responsive">
                                    </div>
                                    <div class="pull-left info clear">
                                        <p>IGU - Foz do Iguaçu</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>CWB - Curitiba</p>
                                        <p>Saídas</p>
                                        <p>30/00/00</p>
                                        <p>05h00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Base T</p>
                                        <p>Dur: 00</p>
                                    </div>
                                    <div class="pull-left info">
                                        <p>Escalas 0</p>
                                    </div>
                                </li>
                            </ul>
                            <div class="total">
                                <h3>Valor do Aéreo</h3>

                                <p class="price">
                                    R$<strong> 4.919,00</strong> 
                                </p>

                                <p class="obs">
                                    <strong>OBS:</strong> não incluso taxas e encargos
                                </p>

                                <button type="submit" class="btn btn-default btnred">
                                    <p>Selecionar</p>
                                    <span class="glyphicon glyphicon-pushpin"></span>
                                </button>
                            </div>
                        </div>
                        <?php endfor; ?>
                        
                        
                    </div>
                    
                </div><!-- resultado -->
            </div>
            
        </div><!-- row -->
    </div><!-- container -->
    
    
    
    
    
</div><!-- .resultbusca -->            
<?php include 'footer.php';?>